# Randvidious
This Firefox Extension forwards youtube links to Invidious, but picks a different random instance for each link.
It also forwards the main youtube page to a random Instance.
The Addon is free and does not collect any Data from you.

## Disclaimer
Use it at your own risk!
Im not responsible for the Content or Security of the Instances you'll be forwarded to.
You can find a list of the Instances used [here](https://codeberg.org/HanSolo97/randvidious#instances).

## Features
- Redirection to a random instance
- On/Off switch
- polling list of Instances with high availability from the Invidious API
- periodically polling, for now every 60mins
- "get instance" - Button for opening a random Instance without a link.

## Future planned features
- fixed instance setting
- blacklist (instances might still be slow, even with good health status...)
- pass some settings to Invidious, such as autoplay

## Installation
The addon is now available in Firefox Addon Store.
The Addon is only available for manual install in Chrome, because i dont want to have a Google Account.

### Manual Installation

#### Firefox (only until Browser restart, better install from Firefox Addons)
1. Download or git clone the whole code
2. Unpack, if you downloaded it as a ZIP
3. Enter about:debugging in the URL bar
4. click "This Firefox"
5. click "Load Temporary Addon"
6. Select the manifest.json file in the source folder.
7. Enjoy!

#### Chrome (only manual installation supported)
1. Enter chrome://extensions in adress bar.
2. Enable developer mode
3. Load unzipped extension from local storage by selecting the folder.

## License
The Source Code is licensed under AGPL 3.0, see [LICENSE](https://codeberg.org/HanSolo97/randvidious/src/branch/main/LICENSE) file for more information.

## Icons
- Favicons made by [srip](https://www.flaticon.com/authors/srip) from [www.flaticon.com](https://www.flaticon.com/)

## Instances
You can find the instances used [here](https://docs.invidious.io/instances/), but only healthy ones are forwarded to.
Note that the list might change from time to time, as those are the public available instances.