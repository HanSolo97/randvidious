document.addEventListener("DOMContentLoaded", function() {
btEnable = document.getElementById("poweronoff");
btGetInst = document.getElementById("binst")

document.querySelector("form").addEventListener("submit", change_enabled);
btGetInst.addEventListener("click", getInst);

var instance = [];
var instances = [];
var newUrl;

chrome.storage.local.get(['enabled'], function (enabled) {
  btEnable.checked = enabled.enabled;
});

//get instances, yep doubled cause the function couldnt be imported from content script
chrome.storage.local.get(['inst'], function (inst) {
  instances = inst.inst
  console.log(instances)

  do {
    instance = instances[Math.floor(Math.random() * instances.length)]
    console.log(instance)
  } while ( instance[0].includes(".onion") || instance[0].includes(".i2p") ) 

  newUrl = "https://" + instance[0]
});

async function change_enabled(event) {
  event.preventDefault(); // Prevent form submission
  var checked = btEnable.checked;

  // acquire permission for host youtube.com
  await chrome.permissions.request({
    origins: ["*://*.youtube.com/*"]
  }, function (granted) {
    if (granted) {
      console.log("Permission was granted");
    } else {
      console.log("Permission was denied");
    }
  });

  chrome.storage.local.set({ enabled: checked }, function () {
    console.log('Value is set to ' + checked);
  });
}

function getInst() {
  window.open(newUrl, '_blank');
}

});