//get instances textfile from github via Http Request
var instance = [];
var instances = [];

function checkForward() {
    do {
        instance = instances[Math.floor(Math.random() * instances.length)]
        console.log(instance)
        // Catch Empty Status
        var status = (instance[1].monitor ? instance[1].monitor.statusClass : "nostatus")
    } while (instance[0].includes(".onion") || status != "success")

    const yt = "youtube.com/watch";
    const ytb = "https://www.youtube.com/";
    // Youtube Main Page?
    if (document.URL == ytb) {
        var newUrl = "https://" + instance[0];
        location.href = newUrl;
    }
    // Video link?
    else if (document.URL.includes(yt)) {
        var ursplit = document.URL.split("/watch");
        var newUrl = "https://" + instance[0] + '/watch' + ursplit[1];
        location.href = newUrl;
    } else {
        // Be quiet.
    }
}

// Lets start the Callback hell, otherwise, the functions will overtake each other
chrome.storage.local.get(['enabled'], function (enabled) {
    console.log(enabled)

    if (enabled.enabled) {
        chrome.storage.local.get(['inst'], function (inst) {
            instances = inst.inst
            console.log(instances)
            checkForward()
        });
    }
});