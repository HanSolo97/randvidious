btEnable = document.getElementById("poweronoff");
btGetInst = document.getElementById("binst")

var instance = [];
var instances = [];
var newUrl;

//get instances, yep doubled cause the function couldnt be imported from content script
chrome.storage.local.get(['inst'], function (inst) {
  instances = inst.inst
  console.log(instances)

  do {
    instance = instances[Math.floor(Math.random() * instances.length)]
    console.log(instance)
    // Catch Empty Status
    var status = (instance[1].monitor ? instance[1].monitor.statusClass : "nostatus")
  } while (instance[0].includes(".onion") || status != "success")

  newUrl = "https://" + instance[0]
});

function readSettings() {
  chrome.storage.local.get(['enabled'], function (enabled) {
    btEnable.checked = enabled.enabled;
  });
}

async function change_enabled() {
  var checked;
  checked = btEnable.checked;

  // acquire permission for host youtube.com
  await chrome.permissions.request({
    origins: ["*://*.youtube.com/*"]
  }, function (granted) {
    if (granted) {
      console.log("Permission was granted");
    } else {
      console.log("Permission was denied");
    }
  });

  chrome.storage.local.set({ enabled: checked }, function () {
    console.log('Value is set to ' + checked);
  });
}

function getInst() {
  window.open(newUrl, '_blank');
}

document.addEventListener("DOMContentLoaded", readSettings);
document.querySelector("form").addEventListener("submit", change_enabled);
btGetInst.addEventListener("click", getInst);