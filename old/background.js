var instances = []

function fetchInst(callback) {
    fetch("https://api.invidious.io/instances.json?pretty=1&sort_by=health")
        .then(response => response.json())
        .then(inst => {
            inst.forEach(function (item) {
                instances.push(item);
            })
            callback()
        }).catch(function (error) {
            console.log("error: " + error);
        });
}

chrome.runtime.onInstalled.addListener(() => {
    console.log('onInstalled...');
    // create alarm after extension is installed / upgraded
    chrome.alarms.create('refresh', { periodInMinutes: 60 });

    if (instances.length == 0) {
        console.log("Initial refresh"); // refresh
        fetchInst(function () {
            chrome.storage.local.set({ inst: instances }, function () {
                console.log('Value is set to ' + instances);
            });
        })
    }
});

chrome.alarms.onAlarm.addListener((alarm) => {
    console.log(alarm.name); // refresh
    instances = []
    fetchInst(function () {
        chrome.storage.local.set({ inst: instances }, function () {
            console.log('Value is set to ' + instances);
        });
    })
});